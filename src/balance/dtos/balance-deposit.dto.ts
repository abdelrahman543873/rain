import { IsInt, IsNumber } from 'class-validator';

export class BalanceDepositDto {
  @IsInt()
  coin: number;

  @IsNumber()
  balance: number;
}
