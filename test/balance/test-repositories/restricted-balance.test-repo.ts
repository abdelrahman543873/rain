import { RestrictedBalanceRepository } from '../../../src/balance/repositories/restricted-balance.repository';

export const restrictedBalanceTestRepo = (): RestrictedBalanceRepository =>
  <RestrictedBalanceRepository>global.restrictedBalanceRepository;
