import { Allow, IsInt, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { User } from '../../user/entities/user.entity';
import { IsSufficientBalance } from '../validators/is-suffecient-balance.validator';
import { IsWagerRequirementMet } from '../validators/is-wager-requirement-met.validator';
import { IsMoreThanDayLimit } from '../../transaction/validators/is-more-than-day-limit.validator';

export class BalanceWithdrawDto {
  @IsWagerRequirementMet()
  @IsSufficientBalance()
  @IsInt()
  coin: number;

  @IsMoreThanDayLimit()
  @IsNumber()
  balance: number;

  @ApiProperty({ readOnly: true })
  @Allow()
  user?: User;
}
