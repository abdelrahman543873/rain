import { userFactory } from './../user/user.factory';
import { rainTestService } from './rain.test-service';
import { wagerFactory } from '../wager/wager.factory';
import { faker } from '@faker-js/faker';
import { restrictedBalanceTestRepo } from '../balance/test-repositories/restricted-balance.test-repo';

describe('rain suite case', () => {
  it('should distribute rain correctly', async () => {
    const rain = faker.number.int({ max: 50000 });
    const firstUser = await userFactory();
    const secondUser = await userFactory();
    //first User Wager
    const firstUserWager = await wagerFactory({
      user: { id: firstUser.id },
      balance: 20,
      createdAt: new Date(),
    });
    //second User Wager
    const secondUserWager = await wagerFactory({
      user: { id: secondUser.id },
      balance: 10,
      createdAt: new Date(),
    });
    // rain
    await rainTestService().rainExecution(rain);
    // total balances distributed should be equal to rain amount
    const firstUserAwardedBalances = (
      await restrictedBalanceTestRepo().find({
        where: { user: { id: firstUser.id } },
      })
    ).map((balance) => +balance.balance);
    const secondUserAwardedBalances = (
      await restrictedBalanceTestRepo().find({
        where: { user: { id: secondUser.id } },
      })
    ).map((balance) => +balance.balance);
    const totalBalances = [
      ...firstUserAwardedBalances,
      ...secondUserAwardedBalances,
    ];
    let distributedTotal = 0;
    totalBalances.forEach((balance) => {
      distributedTotal += balance;
    });
    expect(+distributedTotal.toFixed(0)).toBe(rain);
    // first and second user should be included in the 50% distribution
    // 4 here refers to the fact that the rain will be divided by 50% for distribution
    // and will be divided by 2 "the number of users"
    expect(firstUserAwardedBalances).toContain(rain / 4);
    expect(secondUserAwardedBalances).toContain(rain / 4);
    // first and second user should get distributions according to how much they have bet of the total bet
    expect(firstUserAwardedBalances).toContain(
      +(
        (firstUserWager.balance /
          (firstUserWager.balance + secondUserWager.balance)) *
        (rain / 2)
      ),
    );
    expect(secondUserAwardedBalances).toContain(
      +(
        (secondUserWager.balance /
          (firstUserWager.balance + secondUserWager.balance)) *
        (rain / 2)
      ),
    );
  });
});
