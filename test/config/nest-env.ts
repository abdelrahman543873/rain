import { TestingModule } from '@nestjs/testing';
import NodeEnvironment from 'jest-environment-node';
import { BalanceRepository } from '../../src/balance/repositories/balance.repository';
import { WagerRepository } from '../../src/wager/wager.repository';
import { UserRepository } from '../../src/user/user.repository';
import { CoinRepository } from '../../src/coin/coin.repository';
import { RainJob } from '../../src/jobs/rain.job';
import { RestrictedBalanceRepository } from '../../src/balance/repositories/restricted-balance.repository';

class TestEnvironment extends NodeEnvironment {
  constructor(config, context) {
    super(config, context);
  }

  async setup() {
    await super.setup();
    this.global.app = global.app;
    const app: TestingModule = <TestingModule>this.global.app;
    this.global.balanceRepository =
      app.get<BalanceRepository>(BalanceRepository);
    this.global.wagerRepository = app.get<WagerRepository>(WagerRepository);
    this.global.userRepository = app.get<UserRepository>(UserRepository);
    this.global.coinRepository = app.get<CoinRepository>(CoinRepository);
    this.global.rainJob = app.get<RainJob>(RainJob);
    this.global.restrictedBalanceRepository =
      app.get<RestrictedBalanceRepository>(RestrictedBalanceRepository);
  }

  async teardown() {
    await super.teardown();
  }
}

export default TestEnvironment;
