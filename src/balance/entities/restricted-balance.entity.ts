import { User } from '../../user/entities/user.entity';
import { Coin } from '../../coin/entities/coin.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class RestrictedBalance {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @ManyToOne(() => Coin, (coin) => coin.balances)
  coin: Coin;

  @Column({ type: 'double precision' })
  balance: number;

  @ManyToOne(() => User, (user) => user.balances)
  user: User;

  @Column({ nullable: true })
  unlockingWagerAmount?: number;

  @Column({ nullable: true })
  claimedAt?: Date;

  @Column({ nullable: true })
  awardedAt?: Date;

  @Column({ nullable: true })
  expiredAt?: Date;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
