import { IsInt } from 'class-validator';

export class DiscardRestrictedBalanceDto {
  @IsInt()
  restrictedBalanceId: number;
}
