import { userFactory } from './../user/user.factory';
import { HTTP_METHODS_ENUM } from '../config/request.methods.enum';
import { WITHDRAW } from '../endpoints/balance.endpoints';
import { testRequest } from '../config/request';
import { faker } from '@faker-js/faker';
import { balanceFactory } from './balance.factory';
import { balanceTestRepo } from './test-repositories/balance.test-repo';

describe('balance withdraw suite case', () => {
  it('should withdraw successfully', async () => {
    const user = await userFactory();
    const balance = await balanceFactory({ user: { id: user.id } });
    const balanceInput = faker.number.int({ max: balance.balance, min: 0 });
    const response = await testRequest({
      method: HTTP_METHODS_ENUM.POST,
      url: WITHDRAW,
      variables: {
        coin: balance.coin.id,
        balance: balanceInput,
      },
      token: user.token,
    });
    expect(response.body.affected).toBe(1);
    const balanceAfterWithdrawal = await balanceTestRepo().findOne({
      where: { user: { id: user.id }, coin: { id: balance.coin.id } },
    });
    expect(balanceAfterWithdrawal.balance).toBe(balance.balance - balanceInput);
  });

  it("shouldn't withdraw if unrestricted balance isn't enough", async () => {
    const user = await userFactory();
    const balance = await balanceFactory({ user: { id: user.id } });
    const balanceInput = faker.number.int({ min: balance.balance });
    const response = await testRequest({
      method: HTTP_METHODS_ENUM.POST,
      url: WITHDRAW,
      variables: {
        coin: balance.coin.id,
        balance: balanceInput,
      },
      token: user.token,
    });
    expect(response.body.message[0]).toContain('not enough balance');
  });

  it("shouldn't withdraw if transaction of deposits are bigger than 5000", async () => {
    const user = await userFactory();
    const balance = await balanceFactory({
      user: { id: user.id },
      balance: 5000,
    });
    const balanceInput = faker.number.int({ min: balance.balance });
    const response = await testRequest({
      method: HTTP_METHODS_ENUM.POST,
      url: WITHDRAW,
      variables: {
        coin: balance.coin.id,
        balance: balanceInput,
      },
      token: user.token,
    });
    expect(response.body.message[0]).toContain(
      'transaction need to be reviewed by admin',
    );
  });
});
