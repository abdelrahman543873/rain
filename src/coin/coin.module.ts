import { Module } from '@nestjs/common';
import { CoinRepository } from './coin.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Coin } from './entities/coin.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Coin])],
  providers: [CoinRepository],
})
export class CoinModule {}
