import { User } from '../../src/user/entities/user.entity';
import { faker } from '@faker-js/faker';
import { userTestRepo } from './user.test-repo';
import { hashPassSync } from '../../src/shared/utils/bcrypt.functions';
import { generateAuthToken } from '../../src/shared/utils/generate-auth-tokens';

interface IUser {
  username?: string;
  email?: string;
  xp?: number;
  streak_xp?: number;
  password?: string;
}

export const buildUsersParams = (obj: IUser = {}): IUser => {
  return {
    username: obj.username || faker.internet.userName(),
    email: obj.email || faker.internet.email(),
    xp: obj.xp || faker.number.int({ max: 100 }),
    streak_xp: obj.streak_xp || faker.number.int({ max: 100 }),
    password: obj.password || faker.internet.password(),
  };
};

export const userFactory = async (obj: IUser = {}): Promise<User> => {
  const params: IUser = buildUsersParams(obj);
  const user: User = await userTestRepo().save({
    ...params,
    password: hashPassSync(params.password),
  });
  user.token = generateAuthToken(user);
  return user;
};
