import { Injectable } from '@nestjs/common';
import { TransactionRepository } from './transaction.repository';

@Injectable()
export class TransactionService {
  constructor(private readonly transactionRepository: TransactionRepository) {}

  async checkWithdrawalAndDepositDifference(userId: number) {
    const today = new Date();
    today.setHours(0, 0, 0, 0);

    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);

    const withdrawalAmount =
      await this.transactionRepository.getDailyTransactionAmount(
        userId,
        today,
        tomorrow,
        'withdrawal',
      );
    const depositAmount =
      await this.transactionRepository.getDailyTransactionAmount(
        userId,
        today,
        tomorrow,
        'deposit',
      );
    return Math.abs(withdrawalAmount - depositAmount);
  }
}
