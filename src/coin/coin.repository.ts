import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Coin } from './entities/coin.entity';
import { Repository } from 'typeorm';
import { TypeormRepository } from '../shared/abstract/typeorm-repository.abstract';

@Injectable()
export class CoinRepository extends TypeormRepository<Coin> {
  constructor(
    @InjectRepository(Coin)
    private readonly coin: Repository<Coin>,
  ) {
    super(coin);
  }
}
