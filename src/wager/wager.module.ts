import { Module } from '@nestjs/common';
import { WagerRepository } from './wager.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Wager } from './entities/wager.entity';
import { WagerController } from './wager.controller';
import { WagerService } from './wager.service';
import { BalanceModule } from '../balance/balance.module';

@Module({
  imports: [TypeOrmModule.forFeature([Wager]), BalanceModule],
  providers: [WagerRepository, WagerService],
  controllers: [WagerController],
  exports: [WagerRepository],
})
export class WagerModule {}
