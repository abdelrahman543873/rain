import { User } from '../../user/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Transaction,
  OneToOne,
} from 'typeorm';

@Entity()
export class Approval {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @OneToOne(() => Transaction)
  transaction: Transaction;

  @OneToOne(() => User)
  reviewedBy: User;

  @Column()
  approvalStatus: string;
}
