import { IsInt, IsNumber } from 'class-validator';

export class RewardDto {
  @IsNumber()
  multiplier: number;

  @IsInt()
  wager: number;
}
