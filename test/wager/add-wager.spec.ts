import { userFactory } from './../user/user.factory';
import { HTTP_METHODS_ENUM } from '../config/request.methods.enum';
import { testRequest } from '../config/request';
import { coinFactory } from '../coin/coin.factory';
import { faker } from '@faker-js/faker';
import { WAGER } from '../endpoints/wager.endpoints';

describe('wager suite case', () => {
  it('should add wager successfully', async () => {
    const user = await userFactory();
    const coin = await coinFactory();
    const balance = faker.number.int();
    const response = await testRequest({
      method: HTTP_METHODS_ENUM.POST,
      url: WAGER,
      variables: {
        coin: coin.id,
        balance,
      },
      token: user.token,
    });
    expect(response.body.balance).toBe(balance);
  });
});
