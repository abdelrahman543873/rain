import { UserRepository } from './../../src/user/user.repository';

export const userTestRepo = (): UserRepository =>
  <UserRepository>global.userRepository;
