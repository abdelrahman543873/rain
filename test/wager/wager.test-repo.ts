import { WagerRepository } from '../../src/wager/wager.repository';

export const wagerTestRepo = (): WagerRepository =>
  <WagerRepository>global.wagerRepository;
