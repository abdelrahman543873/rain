import { Module } from '@nestjs/common';
import { ApprovalService } from './approval.service';
import { ApprovalRepository } from './approval.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Approval } from './entities/approval.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Approval])],
  providers: [ApprovalService, ApprovalRepository],
})
export class ApprovalModule {}
