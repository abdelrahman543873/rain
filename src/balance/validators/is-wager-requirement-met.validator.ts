import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { User } from '../../user/entities/user.entity';
import { RestrictedBalanceRepository } from '../repositories/restricted-balance.repository';

@ValidatorConstraint({ name: 'WagerRequirementMetValidator', async: true })
@Injectable()
export class WagerRequirementMetValidator
  implements ValidatorConstraintInterface
{
  constructor(
    private readonly restrictedBalanceRepository: RestrictedBalanceRepository,
  ) {}
  async validate(
    coin: number,
    validationArguments: ValidationArguments,
  ): Promise<boolean> {
    const currentUser: User = JSON.parse(validationArguments.object['user']);
    const restrictedCoinBalance =
      await this.restrictedBalanceRepository.getRestrictedBalance({
        userId: currentUser.id,
        coinId: coin,
      });
    if (restrictedCoinBalance.unlockingWagerAmount > 0) return false;
    return true;
  }

  defaultMessage() {
    return 'wager requirement not met';
  }
}

export function IsWagerRequirementMet(validationOptions?: ValidationOptions) {
  return function (object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: WagerRequirementMetValidator,
    });
  };
}
