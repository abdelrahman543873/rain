import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Transaction } from './entities/transaction.entity';
import { Repository, Between } from 'typeorm';
import { TypeormRepository } from '../shared/abstract/typeorm-repository.abstract';

@Injectable()
export class TransactionRepository extends TypeormRepository<Transaction> {
  constructor(
    @InjectRepository(Transaction)
    private readonly transaction: Repository<Transaction>,
  ) {
    super(transaction);
  }

  getDailyTransactionAmount(
    userId: number,
    startDate: Date,
    endDate: Date,
    type: string,
  ) {
    return this.transaction.sum('amount', {
      user: { id: userId },
      createdAt: Between(startDate, endDate),
      transactionType: type,
    });
  }
}
