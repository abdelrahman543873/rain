import { userFactory } from './../user/user.factory';
import { User } from '../../src/user/entities/user.entity';
import { faker } from '@faker-js/faker';
import { Coin } from '../../src/coin/entities/coin.entity';
import { Balance } from '../../src/balance/entities/balance.entity';
import { balanceTestRepo } from './test-repositories/balance.test-repo';
import { coinFactory } from '../coin/coin.factory';

interface IBalance {
  coin?: Partial<Coin>;
  balance?: number;
  user?: Partial<User>;
}

export const buildBalanceParams = async (
  obj: IBalance = {},
): Promise<IBalance> => {
  return {
    coin: obj.coin || { id: (await coinFactory()).id },
    balance: obj.balance || faker.number.int(),
    user: obj.user || { id: (await userFactory()).id },
  };
};

export const balanceFactory = async (obj: IBalance = {}): Promise<Balance> => {
  const params: IBalance = await buildBalanceParams(obj);
  return await balanceTestRepo().save(params);
};
