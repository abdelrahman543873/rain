import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TypeormRepository } from '../../shared/abstract/typeorm-repository.abstract';
import { RestrictedBalance } from '../entities/restricted-balance.entity';
import { BalanceWithdrawDto } from '../dtos/balance-withdraw.dto';

@Injectable()
export class RestrictedBalanceRepository extends TypeormRepository<RestrictedBalance> {
  constructor(
    @InjectRepository(RestrictedBalance)
    private readonly restrictedBalance: Repository<RestrictedBalance>,
  ) {
    super(restrictedBalance);
  }

  getRestrictedBalance({ userId, coinId }: { userId: number; coinId: number }) {
    return this.restrictedBalance.findOne({
      where: { user: { id: userId }, coin: { id: coinId } },
    });
  }

  addBalance(userId: number, coinId: number, balance: number) {
    return this.restrictedBalance.increment(
      { user: { id: userId }, coin: { id: coinId } },
      'balance',
      balance,
    );
  }

  withdrawRestrictedBalance(
    userId: number,
    balanceWithdrawDto: BalanceWithdrawDto,
  ) {
    return this.restrictedBalance.decrement(
      { user: { id: userId } },
      'balance',
      balanceWithdrawDto.balance,
    );
  }

  addRestrictedBalanceToUsers({
    users,
    balance,
  }: {
    users: number[];
    balance: number;
  }) {
    const restrictedBalances = users.map((user) => {
      return {
        user: {
          id: user,
        },
        balance,
        awardedAt: new Date(),
      };
    });
    return this.restrictedBalance.insert(restrictedBalances);
  }

  addRestrictedBalancesToUsers({
    users,
    balances,
  }: {
    users: number[];
    balances: number[];
  }) {
    const restrictedBalances = users.map((user, index) => {
      return {
        user: {
          id: user,
        },
        balance: balances[index],
        awardedAt: new Date(),
      };
    });
    return this.restrictedBalance.insert(restrictedBalances);
  }

  reduceUnlockingWagerAmount({
    coinId,
    userId,
    wager,
  }: {
    coinId: number;
    userId: number;
    wager: number;
  }) {
    return this.restrictedBalance.decrement(
      { user: { id: userId }, coin: { id: coinId } },
      'unlockingWagerAmount',
      wager,
    );
  }

  getRestrictedCoinBalance(userId: number, coinId: number) {
    return this.restrictedBalance.findOne({
      where: { user: { id: userId }, coin: { id: coinId } },
    });
  }
}
