import { User } from '../../user/entities/user.entity';
import { sign } from 'jsonwebtoken';

export const generateAuthToken = (user: Partial<User>): string => {
  return sign(user, process.env.JWT_SECRET);
};
