import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { BalanceRepository } from '../balance/repositories/balance.repository';
import { WagerRepository } from '../wager/wager.repository';
import { RestrictedBalanceRepository } from '../balance/repositories/restricted-balance.repository';

@Injectable()
export class RainJob {
  constructor(
    private readonly wagerRepository: WagerRepository,
    private readonly restrictedBalanceRepository: RestrictedBalanceRepository,
  ) {}
  private rainAmount = +process.env.RAIN_AMOUNT;

  @Cron('0 * * * *')
  rain() {
    const randomMinute1 = Math.floor(Math.random() * 60);
    const randomMinute2 = Math.floor(Math.random() * 60);

    setTimeout(async () => {
      await this.rainExecution(this.rainAmount);
    }, randomMinute1 * 60 * 1000);

    setTimeout(async () => {
      await this.rainExecution(this.rainAmount);
    }, randomMinute2 * 60 * 1000);
  }

  async rainExecution(rain: number) {
    // distribution of 50% of the wagers to active users
    const wagersWithinTheLast30Minutes =
      await this.wagerRepository.getWagersWithinMinutesTimeRange(30);
    const usersWithin30Minutes = [
      ...new Set(wagersWithinTheLast30Minutes.map((wager) => wager.user.id)),
    ];
    const activeUsersDistributedBalance =
      rain / (2 * usersWithin30Minutes.length);
    await this.restrictedBalanceRepository.addRestrictedBalanceToUsers({
      users: usersWithin30Minutes,
      balance: activeUsersDistributedBalance,
    });
    // distribution of 50% of the wagers to active betting users according to their bets percentage
    const wagersWithinTheLast120Minutes =
      await this.wagerRepository.getWagersWithinMinutesTimeRange(120);
    const totalWagerWithinTheLast120Minutes =
      await this.wagerRepository.getTotalWagerWithinMinutesTimeRange(120);
    const usersWithin120Minutes = [
      ...new Set(wagersWithinTheLast120Minutes.map((wager) => wager.user.id)),
    ];
    const balances = wagersWithinTheLast120Minutes.map(
      (wager) =>
        (wager.balance / totalWagerWithinTheLast120Minutes) * (rain / 2),
    );
    await this.restrictedBalanceRepository.addRestrictedBalancesToUsers({
      users: usersWithin120Minutes,
      balances,
    });
  }
}
