import { User } from '../../user/entities/user.entity';
import { verify } from 'jsonwebtoken';

export const getUserDataFromToken = (token: string): Partial<User> => {
  let payload: Partial<User> = null;
  try {
    payload = <Partial<User>>verify(token, process.env.JWT_SECRET);
  } catch (error) {
    console.log(error);
  }
  return payload;
};
