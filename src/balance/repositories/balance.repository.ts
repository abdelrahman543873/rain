import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Balance } from '../entities/balance.entity';
import { Repository } from 'typeorm';
import { TypeormRepository } from '../../shared/abstract/typeorm-repository.abstract';
import { BalanceWithdrawDto } from '../dtos/balance-withdraw.dto';
import { BalanceDepositDto } from '../dtos/balance-deposit.dto';
import { DiscardRestrictedBalanceDto } from '../dtos/discard.dto';

@Injectable()
export class BalanceRepository extends TypeormRepository<Balance> {
  constructor(
    @InjectRepository(Balance)
    private readonly balance: Repository<Balance>,
  ) {
    super(balance);
  }

  deposit(userId: number, balanceDepositDto: BalanceDepositDto) {
    return this.balance.save({
      balance: balanceDepositDto.balance,
      coin: { id: balanceDepositDto.coin },
      user: { id: userId },
    });
  }

  withdraw(userId: number, balanceWithdrawDto: BalanceWithdrawDto) {
    return this.balance.decrement(
      { user: { id: userId } },
      'balance',
      balanceWithdrawDto.balance,
    );
  }

  discard(
    userId: number,
    discardRestrictedBalanceDto: DiscardRestrictedBalanceDto,
  ) {
    return this.balance.delete({
      user: { id: userId },
      id: discardRestrictedBalanceDto.restrictedBalanceId,
    });
  }

  getCoinBalance(userId: number, coinId: number) {
    return this.balance.findOne({
      where: { user: { id: userId }, coin: { id: coinId } },
    });
  }

  addBalance(userId: number, coinId: number, balance: number) {
    return this.balance.increment(
      { user: { id: userId }, coin: { id: coinId } },
      'balance',
      balance,
    );
  }
}
