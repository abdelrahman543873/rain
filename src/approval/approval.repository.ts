import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Approval } from './entities/approval.entity';
import { Repository } from 'typeorm';
import { TypeormRepository } from '../shared/abstract/typeorm-repository.abstract';

@Injectable()
export class ApprovalRepository extends TypeormRepository<Approval> {
  constructor(
    @InjectRepository(Approval)
    private readonly approval: Repository<Approval>,
  ) {
    super(approval);
  }

  addTransactionToReview(transaction, approvalStatus) {
    return this.approval.save({ transaction, approvalStatus });
  }
}
