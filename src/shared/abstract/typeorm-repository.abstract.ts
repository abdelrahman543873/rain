import {
  DeepPartial,
  FindManyOptions,
  FindOneOptions,
  Repository,
} from 'typeorm';

export class TypeormRepository<Entity> {
  constructor(private readonly repository: Repository<Entity>) {}

  save(input: DeepPartial<Entity>): Promise<Entity> {
    return this.repository.save(input);
  }

  find(input: FindManyOptions<Entity>): Promise<Entity[]> {
    return this.repository.find(input);
  }

  findOne(input: FindOneOptions<Entity>): Promise<Entity> {
    return this.repository.findOne(input);
  }

  clear() {
    return this.repository.clear();
  }
}
