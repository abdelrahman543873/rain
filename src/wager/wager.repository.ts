import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Wager } from './entities/wager.entity';
import { Between, Repository } from 'typeorm';
import { TypeormRepository } from '../shared/abstract/typeorm-repository.abstract';
import { AddWagerDto } from './dtos/add-wager.dto';

@Injectable()
export class WagerRepository extends TypeormRepository<Wager> {
  constructor(
    @InjectRepository(Wager)
    private readonly wager: Repository<Wager>,
  ) {
    super(wager);
  }

  addWager(userId: number, addWagerDto: AddWagerDto) {
    return this.wager.save({
      user: { id: userId },
      balance: addWagerDto.balance,
      coin: { id: addWagerDto.coin },
      ...(addWagerDto.balancePercentage && {
        balancePercentage: addWagerDto.balancePercentage,
      }),
    });
  }

  getWager(id: number) {
    return this.wager.findOne({ where: { id } });
  }

  getWagersWithinMinutesTimeRange(minutes: number) {
    const currentTime = new Date();
    return this.wager.find({
      where: {
        createdAt: Between(
          new Date(currentTime.getTime() - minutes * 60 * 1000),
          currentTime,
        ),
      },
      relations: ['user'],
    });
  }

  getTotalWagerWithinMinutesTimeRange(minutes) {
    const currentTime = new Date();
    return this.wager.sum('balance', {
      createdAt: Between(
        new Date(currentTime.getTime() - minutes * 60 * 1000),
        currentTime,
      ),
    });
  }
}
