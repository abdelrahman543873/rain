import { Balance } from '../../balance/entities/balance.entity';
import { Wager } from '../../wager/entities/wager.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'user' })
export class User {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @Column({
    nullable: true,
    unique: true,
    length: 100,
    collation: 'utf8mb4_unicode_ci',
    transformer: {
      to: (value: string | null) => {
        return value ? value.trim() : value;
      },
      from: (value: string) => value,
    },
  })
  username: string;

  @Column({
    nullable: true,
    length: 100,
  })
  password: string;

  @Column({
    nullable: true,
    unique: true,
    length: 100,
    transformer: {
      to: (value: string | null) => {
        return value ? value.toLowerCase().trim() : value;
      },
      from: (value: string) => value,
    },
  })
  email: string;

  @Column({ default: 0 })
  xp: number;

  @Column({ default: 0 })
  streak_xp: number;

  @OneToMany(() => Wager, (wager) => wager.user)
  wagers: Wager[];

  @OneToMany(() => Balance, (balance) => balance.user)
  balances: Balance[];

  token?: string;
}
