import { RequestContext } from './../shared/interfaces/request.context';
import { Body, Controller, Inject, Post, UseGuards } from '@nestjs/common';
import { WagerService } from './wager.service';
import { AddWagerDto } from './dtos/add-wager.dto';
import { REQUEST } from '@nestjs/core';
import { AuthGuard } from '../shared/guards/auth.guard';
import { RewardDto } from './dtos/reward.dto';

@Controller('wager')
export class WagerController {
  constructor(
    private readonly wagerService: WagerService,
    @Inject(REQUEST) private readonly request: RequestContext,
  ) {}

  @UseGuards(AuthGuard)
  @Post()
  async addWager(@Body() addWagerDto: AddWagerDto) {
    return await this.wagerService.addWager(this.request.user.id, addWagerDto);
  }

  @UseGuards(AuthGuard)
  @Post('reward')
  async reward(@Body() balanceDepositDto: RewardDto) {
    return await this.wagerService.reward(balanceDepositDto);
  }
}
