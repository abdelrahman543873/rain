import { User } from '../../user/entities/user.entity';
import { Coin } from '../../coin/entities/coin.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Balance {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @ManyToOne(() => Coin, (coin) => coin.balances)
  coin: Coin;

  @Column({ type: 'double precision' })
  balance: number;

  @ManyToOne(() => User, (user) => user.balances)
  user: User;
}
