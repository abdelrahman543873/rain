import { coinTestRepo } from './coin.test-repo';
import { faker } from '@faker-js/faker';
import { Coin } from '../../src/coin/entities/coin.entity';

interface ICoin {
  name?: string;
}

export const buildCoinParams = (obj: ICoin = {}): ICoin => {
  return {
    name: obj.name || faker.internet.avatar(),
  };
};

export const coinFactory = async (obj: ICoin = {}): Promise<Coin> => {
  const params: ICoin = buildCoinParams(obj);
  return await coinTestRepo().save(params);
};
