import { Balance } from '../../balance/entities/balance.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Wager } from '../../wager/entities/wager.entity';

@Entity()
export class Coin {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @Column()
  name: string;

  @OneToMany(() => Balance, (balance) => balance.coin)
  balances: Balance[];

  @OneToMany(() => Wager, (wager) => wager.coin)
  wagers: Wager[];
}
