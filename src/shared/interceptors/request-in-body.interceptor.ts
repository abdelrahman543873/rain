import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { RequestContext } from '../interfaces/request.context';

@Injectable()
export class RequestInBodyInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request = context.switchToHttp().getRequest() as RequestContext;
    request.body.user = JSON.stringify(request.user);
    return next.handle();
  }
}
