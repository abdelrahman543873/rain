import { WagerRepository } from '../../src/wager/wager.repository';
import { BalanceRepository } from '../../src/balance/repositories/balance.repository';
import { TestingModule } from '@nestjs/testing';

export default async function (): Promise<void> {
  const app: TestingModule = <TestingModule>global.app;
  await clearData(app);
  await app.close();
}

const clearData = async (app: TestingModule) => {
  await app.get(BalanceRepository).clear();
  await app.get(WagerRepository).clear();
};
