import { User } from '../../user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Coin } from '../../coin/entities/coin.entity';

@Entity()
export class Wager {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @ManyToOne(() => Coin, (coin) => coin.wagers)
  coin: Coin;

  @Column({ type: 'bigint' })
  balance: number;

  @Column()
  balancePercentage: number;

  @ManyToOne(() => User, (user) => user.wagers)
  user: User;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
