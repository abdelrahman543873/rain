import {
  Body,
  Controller,
  Delete,
  Inject,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { BalanceService } from './balance.service';
import { BalanceDepositDto } from './dtos/balance-deposit.dto';
import { RequestContext } from '../shared/interfaces/request.context';
import { REQUEST } from '@nestjs/core';
import { AuthGuard } from '../shared/guards/auth.guard';
import { BalanceWithdrawDto } from './dtos/balance-withdraw.dto';
import { RequestInBodyInterceptor } from '../shared/interceptors/request-in-body.interceptor';
import { DiscardRestrictedBalanceDto } from './dtos/discard.dto';

@Controller('balance')
export class BalanceController {
  constructor(
    private readonly balanceService: BalanceService,
    @Inject(REQUEST) private readonly request: RequestContext,
  ) {}

  @UseGuards(AuthGuard)
  @Post('deposit')
  async deposit(@Body() balanceDepositDto: BalanceDepositDto) {
    return await this.balanceService.deposit(
      this.request.user.id,
      balanceDepositDto,
    );
  }

  @UseGuards(AuthGuard)
  @UseInterceptors(RequestInBodyInterceptor)
  @Post('withdraw')
  async withdraw(@Body() balanceDepositDto: BalanceWithdrawDto) {
    return await this.balanceService.withdraw(
      this.request.user.id,
      balanceDepositDto,
    );
  }

  @UseGuards(AuthGuard)
  @UseInterceptors(RequestInBodyInterceptor)
  @Delete('discard-restricted')
  async discardRestricted(
    @Body() discardRestrictedBalanceDto: DiscardRestrictedBalanceDto,
  ) {
    return await this.balanceService.discard(
      this.request.user.id,
      discardRestrictedBalanceDto,
    );
  }

  @UseGuards(AuthGuard)
  @UseInterceptors(RequestInBodyInterceptor)
  @Post('withdraw-restricted')
  async withdrawRestrictedBalance(
    @Body() balanceDepositDto: BalanceWithdrawDto,
  ) {
    return await this.balanceService.withdraw(
      this.request.user.id,
      balanceDepositDto,
    );
  }
}
