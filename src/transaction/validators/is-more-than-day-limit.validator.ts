import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { User } from '../../user/entities/user.entity';
import { TransactionService } from '../transaction.service';

@ValidatorConstraint({ name: 'IsMoreThanDayLimitValidator', async: true })
@Injectable()
export class IsMoreThanDayLimitValidator
  implements ValidatorConstraintInterface
{
  constructor(private readonly transactionService: TransactionService) {}
  async validate(
    amount: number,
    validationArguments: ValidationArguments,
  ): Promise<boolean> {
    const currentUser: User = JSON.parse(validationArguments.object['user']);
    const dailyTransactionalDifference =
      await this.transactionService.checkWithdrawalAndDepositDifference(
        currentUser.id,
      );
    if (dailyTransactionalDifference > 5000) return false;
    return true;
  }

  defaultMessage() {
    return 'transaction need to be reviewed by admin';
  }
}

export function IsMoreThanDayLimit(validationOptions?: ValidationOptions) {
  return function (object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsMoreThanDayLimitValidator,
    });
  };
}
