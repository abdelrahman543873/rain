import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { RequestContext } from '../interfaces/request.context';
import { getAuthTokenFromRequestHeaders } from '../utils/get-auth-token-from-header.util';
import { getUserDataFromToken } from '../utils/unsign-token.util';

@Injectable()
export class AuthGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest<RequestContext>();
    const token = getAuthTokenFromRequestHeaders(request.headers);
    if (!token) throw new Error('unauthorized');
    const userData = getUserDataFromToken(token);
    if (!userData || !userData.id) {
      throw new Error('unauthorize');
    }
    request.user = userData;
    return true;
  }
}
