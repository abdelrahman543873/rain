import { userFactory } from './../user/user.factory';
import { User } from '../../src/user/entities/user.entity';
import { faker } from '@faker-js/faker';
import { Coin } from '../../src/coin/entities/coin.entity';
import { coinFactory } from '../coin/coin.factory';
import { wagerTestRepo } from './wager.test-repo';
import { Wager } from '../../src/wager/entities/wager.entity';

interface IWager {
  coin?: Partial<Coin>;
  balance?: number;
  user?: Partial<User>;
  balancePercentage?: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export const buildWagerParams = async (obj: IWager = {}): Promise<IWager> => {
  return {
    coin: obj.coin || { id: (await coinFactory()).id },
    balance: obj.balance || faker.number.int(),
    user: obj.user || { id: (await userFactory()).id },
    balancePercentage:
      obj.balancePercentage || faker.number.int({ max: 100, min: 0 }),
    ...(obj.createdAt && { createdAt: obj.createdAt }),
    ...(obj.updatedAt && { updatedAt: obj.updatedAt }),
  };
};

export const wagerFactory = async (obj: IWager = {}): Promise<Wager> => {
  const params: IWager = await buildWagerParams(obj);
  return await wagerTestRepo().save(params);
};
