import { Injectable } from '@nestjs/common';
import { WagerRepository } from './wager.repository';
import { AddWagerDto } from './dtos/add-wager.dto';
import { RestrictedBalanceRepository } from '../balance/repositories/restricted-balance.repository';
import { BalanceRepository } from '../balance/repositories/balance.repository';
import { RewardDto } from './dtos/reward.dto';

@Injectable()
export class WagerService {
  constructor(
    private readonly wagerRepository: WagerRepository,
    private readonly restrictedBalanceRepository: RestrictedBalanceRepository,
    private readonly balanceRepository: BalanceRepository,
  ) {}
  async addWager(userId: number, addWagerDto: AddWagerDto) {
    const restrictedBalance =
      await this.restrictedBalanceRepository.getRestrictedBalance({
        userId,
        coinId: addWagerDto.coin,
      });
    if (restrictedBalance && restrictedBalance.unlockingWagerAmount > 0)
      await this.restrictedBalanceRepository.reduceUnlockingWagerAmount({
        userId,
        coinId: addWagerDto.coin,
        wager: addWagerDto.balance,
      });
    const balance = await this.balanceRepository.getCoinBalance(
      userId,
      addWagerDto.coin,
    );
    if (addWagerDto.balance <= balance.balance)
      await this.balanceRepository.withdraw(userId, {
        balance: addWagerDto.balance,
        coin: addWagerDto.coin,
      });
    let balancePercentage = 0;
    if (addWagerDto.balance >= balance.balance) {
      balancePercentage =
        100 - +(restrictedBalance.balance / balance.balance).toFixed(1);
      await this.balanceRepository.withdraw(userId, {
        balance: balance.balance,
        coin: addWagerDto.coin,
      });
      await this.restrictedBalanceRepository.withdrawRestrictedBalance(userId, {
        balance: addWagerDto.balance - balance.balance,
        coin: addWagerDto.coin,
      });
    }
    return await this.wagerRepository.addWager(userId, {
      ...addWagerDto,
      balancePercentage,
    });
  }

  async reward(rewardDto: RewardDto) {
    const wager = await this.wagerRepository.getWager(rewardDto.wager);
    await this.balanceRepository.addBalance(
      wager.user.id,
      wager.coin.id,
      wager.balancePercentage * rewardDto.multiplier * wager.balance,
    );
    await this.restrictedBalanceRepository.addBalance(
      wager.user.id,
      wager.coin.id,
      (100 - wager.balancePercentage) * rewardDto.multiplier * wager.balance,
    );
  }
}
