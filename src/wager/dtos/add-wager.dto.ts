import { IsInt, IsNumber } from 'class-validator';

export class AddWagerDto {
  @IsInt()
  coin: number;

  @IsNumber()
  balance: number;

  balancePercentage?: number;
}
