import { Injectable } from '@nestjs/common';
import { BalanceRepository } from './repositories/balance.repository';
import { BalanceDepositDto } from './dtos/balance-deposit.dto';
import { BalanceWithdrawDto } from './dtos/balance-withdraw.dto';
import { RestrictedBalanceRepository } from './repositories/restricted-balance.repository';
import { DiscardRestrictedBalanceDto } from './dtos/discard.dto';
import { TransactionRepository } from '../transaction/transaction.repository';

@Injectable()
export class BalanceService {
  constructor(
    private readonly balanceRepository: BalanceRepository,
    private readonly restrictedBalanceRepository: RestrictedBalanceRepository,
    private readonly transactionRepository: TransactionRepository,
  ) {}

  async deposit(userId: number, balanceDepositDto: BalanceDepositDto) {
    await this.transactionRepository.save({
      amount: balanceDepositDto.balance,
      transactionType: 'deposit',
      user: { id: userId },
    });
    return await this.balanceRepository.deposit(userId, balanceDepositDto);
  }

  withdraw(userId: number, balanceWithdrawDto: BalanceWithdrawDto) {
    return this.balanceRepository.withdraw(userId, balanceWithdrawDto);
  }

  discard(
    userId: number,
    discardRestrictedBalanceDto: DiscardRestrictedBalanceDto,
  ) {
    return this.balanceRepository.discard(userId, discardRestrictedBalanceDto);
  }

  withdrawRestrictedBalance(
    userId: number,
    balanceWithdrawDto: BalanceWithdrawDto,
  ) {
    return this.restrictedBalanceRepository.withdrawRestrictedBalance(
      userId,
      balanceWithdrawDto,
    );
  }
}
