import { Module } from '@nestjs/common';
import { BalanceController } from './balance.controller';
import { BalanceService } from './balance.service';
import { BalanceRepository } from './repositories/balance.repository';
import { Balance } from './entities/balance.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';
import { RestrictedBalance } from './entities/restricted-balance.entity';
import { RestrictedBalanceRepository } from './repositories/restricted-balance.repository';
import { SufficientBalanceValidator } from './validators/is-suffecient-balance.validator';
import { TransactionModule } from '../transaction/transaction.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Balance, RestrictedBalance]),
    ScheduleModule.forRoot(),
    TransactionModule,
  ],
  controllers: [BalanceController],
  providers: [
    BalanceService,
    BalanceRepository,
    RestrictedBalanceRepository,
    SufficientBalanceValidator,
  ],
  exports: [BalanceRepository, RestrictedBalanceRepository],
})
export class BalanceModule {}
