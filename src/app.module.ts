import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { CoinModule } from './coin/coin.module';
import { BalanceModule } from './balance/balance.module';
import { WagerModule } from './wager/wager.module';
import { ConfigurationModule } from './shared/configuration/configuration.module';
import { RainJob } from './jobs/rain.job';
import { TransactionModule } from './transaction/transaction.module';
import { ApprovalModule } from './approval/approval.module';

@Module({
  imports: [
    ConfigurationModule,
    CoinModule,
    UserModule,
    WagerModule,
    BalanceModule,
    TransactionModule,
    ApprovalModule,
  ],
  providers: [RainJob],
})
export class AppModule {}
