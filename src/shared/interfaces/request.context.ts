import { Request } from 'express';
import { User } from '../../user/entities/user.entity';

export interface RequestContext extends Request {
  user: Partial<User>;
}
