import { CoinRepository } from '../../src/coin/coin.repository';

export const coinTestRepo = (): CoinRepository =>
  <CoinRepository>global.coinRepository;
