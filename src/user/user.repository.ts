import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TypeormRepository } from '../shared/abstract/typeorm-repository.abstract';
import { User } from './entities/user.entity';

@Injectable()
export class UserRepository extends TypeormRepository<User> {
  constructor(
    @InjectRepository(User)
    private readonly user: Repository<User>,
  ) {
    super(user);
  }
}
