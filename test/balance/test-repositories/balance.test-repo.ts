import { BalanceRepository } from '../../../src/balance/repositories/balance.repository';

export const balanceTestRepo = (): BalanceRepository =>
  <BalanceRepository>global.balanceRepository;
