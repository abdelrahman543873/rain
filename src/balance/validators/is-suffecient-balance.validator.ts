import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { BalanceRepository } from '../repositories/balance.repository';
import { User } from '../../user/entities/user.entity';

@ValidatorConstraint({ name: 'SufficientBalanceValidator', async: true })
@Injectable()
export class SufficientBalanceValidator
  implements ValidatorConstraintInterface
{
  constructor(private readonly balanceRepository: BalanceRepository) {}
  async validate(
    coin: number,
    validationArguments: ValidationArguments,
  ): Promise<boolean> {
    const currentUser: User = JSON.parse(validationArguments.object['user']);
    const coinBalance = await this.balanceRepository.getCoinBalance(
      currentUser.id,
      coin,
    );
    const restrictedCoinBalance = await this.balanceRepository.getCoinBalance(
      currentUser.id,
      coin,
    );
    if (
      !coinBalance ||
      coinBalance.balance + restrictedCoinBalance.balance <
        validationArguments.object['balance']
    )
      return false;
    return true;
  }

  defaultMessage() {
    return 'there is not enough balance';
  }
}

export function IsSufficientBalance(validationOptions?: ValidationOptions) {
  return function (object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: SufficientBalanceValidator,
    });
  };
}
